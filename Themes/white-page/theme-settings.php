<?php
/*This file contains theme specific settings.*/
/*Please do not edit the following code from here*/
define("DEFAULTLOGO", "Themes/" . ACTIVETHEME . "/sample-logo.png");
/*To Here*/

/*Logo URL*/
define("LOGOURL", DEFAULTLOGO); /*WARNING: Please Read the ReadMe file before editing this setting.*/
define("LOGOALT", "Error Styler Logo");

/*Do you wish to offer site map option*/
$showsitemap=Yes;
define("SITEMAPURL", "ENTERSITEMAPURLHERE");/*Please enter the url of your sitemap here if you have one. Include http://*/

/*Show Footer?*/
$showfooter=Yes;
?>