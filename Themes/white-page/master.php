<?php /*This is the theme master file, it tells Error Styler what the Error Pages should look like*/ require("theme-settings.php"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo TITLE; ?></title>
        <link href="Themes/<?php echo ACTIVETHEME ?>/css/style_main.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="page">
            <div id="header">
                <img src="<?php echo LOGOURL; ?>" alt="<?php echo LOGOALT; ?>"/>        
            </div>
            <div id="content">
                <h3><?php echo DOCUMENTTITLE; ?><hr/></hr></h3>
                <p><?php echo EXPLANATION; ?><br/><br/><br/></p>
                <h3>What To Do...<hr/></h3>
                <ol> 
						<li><span>Chech the URL for typos.</span></li> 
						<li><span>Visit the <a href="<?php echo HOMEURL; ?>">homepage</a>.</span></li> 
						<?php
if ($showsitemap=='Yes') echo "<li><span>Visit our full website <a href=\"sitemap.php \">sitemap here</a>.</span></li>"; ?> 
				</ol>
                <br/>
                <br/>
                <br/>
                 <?php if ($showfooter=='Yes') echo "<div id=\"footer-text\"><p>Error Pages created by <a href=\"http://webdevelopment.netling.co.uk/projects/error-etyler\">Error Styler</a>. Theme by <a href=\"http://webdevelopment.netling.co.uk\">Netling Web Development</a>.</p></div>" ?>
            </div>    
        </div>
    </body>
</html>
