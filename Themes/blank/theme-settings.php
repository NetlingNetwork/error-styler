<?php
/*This file contains all of the settings specific to the theme*/
/*Please do not edit the following code from here*/
define("DEFAULTLOGO", "Themes/" . ACTIVETHEME . "/sample-logo.png");
/*To Here*/

/*Logo URL*/
define("LOGOURL", "Themes/" . ACTIVETHEME . "/sample-logo.png"); /*WARNING: Please Read the ReadMe file before editing this setting.*/
define("LOGOALT", "Error Styler Logo");

/*Do you wish to offer a site map - default Yes (Case Sensitive)*/
$showsitemap=Yes;
/*If you do, what is the URL?*/
define("SITEMAPURL", "ENTERURLHERE");

/*Include backlink to WPDevNet and Error Styler - default Yes (Case Sensitive)*/
$showfooter=Yes;
?>