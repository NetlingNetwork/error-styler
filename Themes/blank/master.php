<?php /*This is the master file, it tells the error pages what to look like*/ ?>
<html>
    <head>
        <title><?php echo TITLE; ?></title>
        <link href="Themes/<?php echo ACTIVETHEME ?>/css/style_main.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="header">
            <div id="logo">
                <img src="<?php echo LOGOURL; ?>" alt="<?php echo LOGOALT; ?>" width="300px" height="65px" />
            </div>
            <div id="title">
                <h1><?php echo DOCUMENTTITLE; ?></h1>
            </div>  
        </div>
        <div id="content-wrapper">
            <div id="explanation">
                <p><?php echo EXPLANATION; ?></p>
                <h5>What To Do...<hr/></h5>
                <ol> 
						<li><span>Chech the URL for typos.</span></li> 
						<li><span>Visit the <a href="<?php echo HOMEURL; ?>">homepage</a>.</span></li> 
						<?php
if ($showsitemap=='Yes') echo "<li><span>Visit our full website <a href=\"sitemap.php \">sitemap here</a>.</span></li>"; ?> 
					</ol>
                
            </div>
        </div>
       <?php if ($showfooter=='Yes') echo "<div id=\"footer-text\"><p>Error Pages created by <a href=\"http://webdevelopment.netling.co.uk/projects/error-etyler\">Error Styler</a>. Theme by <a href=\"http://webdevelopment.netling.co.uk\">Netling Web Development</a>.</p></div>" ?>
    </body>
</html>